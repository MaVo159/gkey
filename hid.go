package main

/*
#cgo LDFLAGS: -lhidapi-libusb
#include <hidapi/hidapi.h>
*/
import "C"
import "unsafe"
import "reflect"
import "time"
import "strconv"
import "fmt"

func init() {
	C.hid_init()
}

type Hid struct {
	device *C.hid_device
}

type TransactionError uint32

func (e TransactionError) Error() (s string) {
	s = "{TransactionError: "
	switch e {
	case ErrTransRspCRC:
		s += "Response corrupted"
	case ErrTransCmdCRC:
		s += "Command corrupted"
	case ErrTransRspStatus:
		s += "Response status error"
	case ErrTransCidMismatch:
		s += "CommandId mismatch"
	default:
		s += strconv.FormatUint(uint64(e), 16)
	}
	s += "}"
	return
}

const (
	ErrTransCmdCRC      TransactionError = 1
	ErrTransRspCRC      TransactionError = 2
	ErrTransRspStatus   TransactionError = 4
	ErrTransCidMismatch TransactionError = 5
)

type CmdError uint32

func (e CmdError) Error() (s string) {
	s = "{CmdError: "
	switch e {
	case ErrCmdOtpEmptySlot:
		s += "Emtpy OTP slot"
	case ErrCmdOtpInvalidSlotId:
		s += "Invalid OTP slot id"
	case ErrCmdNotAuthorized:
		s += "Not authorized"
	case ErrCmdWrongPassword:
		s += "Wrong password"
	default:
		s += strconv.FormatUint(uint64(e), 16)
	}
	s += "}"
	return
}

const (
	ErrCmdOtpEmptySlot     CmdError = CMD_STATUS_SLOT_NOT_PROGRAMMED
	ErrCmdOtpInvalidSlotId CmdError = CMD_STATUS_WRONG_SLOT
	ErrCmdNotAuthorized    CmdError = CMD_STATUS_NOT_AUTHORIZED
	ErrCmdWrongPassword    CmdError = CMD_STATUS_WRONG_PASSWORD
)

func (h *Hid) Close() {
	C.hid_close(h.device)
}

func (h *Hid) Transaction(c *CommandReport, r *ResponseReport) error {
	// Zero command after execution
	defer c.Zero()
	// Zero response CRCs, which reduce entropy of command Payload
	defer Zero(r.CRC[:], r.LastCRC[:])
	// Calculate command CRC
	c.SetCRC()
	// Try sending command and receiving response
	for isnd := 0; true; isnd++ {
		// Send
		C.hid_send_feature_report(h.device, (*C.uchar)(unsafe.Pointer(c)), HID_REPORT_SIZE)
		// Try receiving response
		for ircv := 0; true; ircv++ {
			// Wait for device
			time.Sleep(time.Second / 10)
			// Receive
			C.hid_get_feature_report(h.device, (*C.uchar)(unsafe.Pointer(r)), HID_REPORT_SIZE)
			// Workaround for missing CRC in busy responses
			// TODO: Move after CRC check once fixed
			if r.Status == STATUS_BUSY {
				ircv--
				continue
			}
			// Check for response corruption.
			// Stop receiving in not corrupted.
			if r.CheckCRC() {
				break
			}
			// Stop trying to receive after 3 corrupted responses.
			if ircv >= 3 {
				return ErrTransRspCRC
			}
		}
		// Check if device received corrupted command.
		// Stop sending if not corrupted.
		if c.CheckCRC(r) {
			break
		}
		// Stop trying to send after 3 corrupted command
		if isnd >= 3 {
			return ErrTransCmdCRC
		}
	}
	// Check command id match
	if r.LastCommandId != c.CommandId {
		return ErrTransCidMismatch
	}
	// Check the command status
	if r.LastStatus != 0 {
		return CmdError(r.LastStatus)
	}
	// Check response status
	if r.Status == STATUS_ERROR {
		return ErrTransRspStatus
	}
	return nil
}

func FindKey() (k *NitrokeyPro) {
	device := C.hid_open(0x20a0, 0x4108, nil)
	if device == nil {
		return
	}
	k = new(NitrokeyPro)
	k.device = device
	return
}

const HID_REPORT_SIZE = 65

type CommandReport struct {
	_         byte
	CommandId byte
	Payload   [HID_REPORT_SIZE - 6]byte
	CRC       [4]byte
}

func (c *CommandReport) Zero() {
	c.CommandId = 0
	Zero(c.Payload[:], c.CRC[:])
}

func (c *CommandReport) GetCRC() [4]byte {
	return crc(c.Bytes()[1 : HID_REPORT_SIZE-4])
}

func (c *CommandReport) SetCRC() {
	c.CRC = c.GetCRC()
}

func (c *CommandReport) CheckCRC(r *ResponseReport) bool {
	return c.CRC == r.LastCRC
}

func (c *CommandReport) Bytes() []byte {
	var slice []byte
	header := (*reflect.SliceHeader)(unsafe.Pointer(&slice))
	header.Data = uintptr(unsafe.Pointer(c))
	header.Len = HID_REPORT_SIZE
	header.Cap = HID_REPORT_SIZE
	return slice
}

type ResponseReport struct {
	_             byte
	Status        byte
	LastCommandId byte
	LastCRC       [4]byte
	LastStatus    byte
	Payload       [HID_REPORT_SIZE - 12]byte
	CRC           [4]byte
}

func (r *ResponseReport) Zero() {
	r.Status = 0
	r.LastCommandId = 0
	r.LastStatus = 0
	Zero(r.Payload[:], r.LastCRC[:], r.CRC[:])
}

func (r *ResponseReport) String() string {
	s := "{Status:"
	switch r.Status {
	case STATUS_READY:
		s += "ready"
	case STATUS_BUSY:
		s += "busy"
	case STATUS_ERROR:
		s += "error"
	case STATUS_RECEIVED_REPORT:
		s += "received report"
	}
	s += fmt.Sprintf(" Cmd:%x CmdCRC:%x CmdStatus:", r.LastCommandId, r.LastCRC)
	switch r.LastStatus {
	case CMD_STATUS_OK:
		s += "ok"
	case CMD_STATUS_WRONG_CRC:
		s += "wrong crc"
	case CMD_STATUS_WRONG_SLOT:
		s += "wrong slot"
	case CMD_STATUS_SLOT_NOT_PROGRAMMED:
		s += "slot not programmed"
	case CMD_STATUS_WRONG_PASSWORD:
		s += "wrong password"
	case CMD_STATUS_NOT_AUTHORIZED:
		s += "not authorized"
	case CMD_STATUS_TIMESTAMP_WARNING:
		s += "timestamp warning"
	case CMD_STATUS_NO_NAME_ERROR:
		s += "no name"
	case CMD_STATUS_NOT_SUPPORTED:
		s += "not supported"
	case CMD_STATUS_UNKNOWN_COMMAND:
		s += "unknown command"
	case CMD_STATUS_AES_DEC_FAILED:
		s += "AES decription failed"
	case CMD_STATUS_AES_CREATE_KEY_FAILED:
		s += "AES key creation failed"
	case CMD_STATUS_ERROR_CHANGING_USER_PASSWORD:
		s += "changing user pin failed"
	case CMD_STATUS_ERROR_CHANGING_ADMIN_PASSWORD:
		s += "changing admin pin failed"
	case CMD_STATUS_ERROR_UNBLOCKING_PIN:
		s += "pin unblocking failed"
	}
	s += fmt.Sprintf(" Data:%x CRC:%x", r.Payload, r.CRC)
	return s
}

func (r *ResponseReport) CheckCRC() bool {
	return r.CRC == crc(r.Bytes()[1:HID_REPORT_SIZE-4])
}

func (r *ResponseReport) Bytes() []byte {
	var slice []byte
	header := (*reflect.SliceHeader)(unsafe.Pointer(&slice))
	header.Data = uintptr(unsafe.Pointer(r))
	header.Len = HID_REPORT_SIZE
	header.Cap = HID_REPORT_SIZE
	return slice
}

func crc(d []byte) [4]byte {
	crc := uint32(0xffffffff)
	for o := 0; o < len(d); o += 4 {
		Data := uint32(d[o]) + uint32(d[o+1])<<8 + uint32(d[o+2])<<16 + uint32(d[o+3])<<24
		crc = crc ^ Data
		for i := 0; i < 32; i++ {
			if 0 != crc&0x80000000 {
				crc = (crc << 1) ^ 0x04C11DB7 // Polynomial used in STM32
			} else {
				crc = (crc << 1)
			}
		}
	}
	return [4]byte{byte(crc), byte(crc >> 8), byte(crc >> 16), byte(crc >> 24)}
}
