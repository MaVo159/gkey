package main

import "log"
import "os"
import "bytes"
import "encoding/json"
import "fmt"

func main() {
	log.SetFlags(0)

	k := FindKey()
	if k == nil {
		log.Fatalln("No device found")
	}
	defer k.Close()

	switch {
	case len(os.Args) < 2:
		ParseError("No action specified")
	case os.Args[1] == "list":
		List(k, os.Args[2:])
	case os.Args[1] == "export":
		Export(k)
	case os.Args[1] == "get":
		Get(k, os.Args[2:])
	default:
		ParseError("Unknown action \"" + os.Args[1] + "\"\n")
	}
	return
}

func Get(k *NitrokeyPro, args []string) {
	switch {
	case len(args) < 1:
		ParseError("No slot type and name specified")
	case len(args) < 2:
		ParseError("No slot name specified")
	case args[0] == "totp":
		GetTotp(k, args[1:])
	case args[0] == "pws":
		UnlockPws(k)
		GetPws(k, args[1:])
	default:
		ParseError("Unknown slot type \"" + args[0] + "\"\n")
	}
}

func GetTotp(k *NitrokeyPro, args []string) {
	names, ids, err := k.GetTotpSlotList()
	if err != nil {
		log.Fatalln("Error reading TOTP slots:", err.Error())
	}
	for i, name := range names {
		if name == args[0] {
			AuthenticateUser(k)
			secret, err := k.GetTotp(ids[i])
			if err != nil {
				log.Fatalln("Error generating TOTP:", err.Error())
			}
			fmt.Print(secret)
			log.Println()
			return
		}
	}
	log.Fatalln("No TOTP slot with name \"" + args[0] + "\"")
}

func GetPws(k *NitrokeyPro, args []string) {
	names, ids, err := k.GetPwsSlotList()
	if err != nil {
		log.Fatalln("Error reading password safe slots:", err.Error())
	}
	for i, name := range names {
		if name == args[0] {
			secret, err := k.GetPwsSlotSecret(ids[i])
			if err != nil {
				log.Fatalln("Error reading password:", err.Error())
			}
			fmt.Print(string(bytes.TrimRight(secret[:], "\x00")))
			log.Println()
			return
		}
	}
	log.Fatalln("No password safe slot with name \"" + args[0] + "\"")
}

func Export(k *NitrokeyPro) {
	UnlockPws(k)
	state, err := k.Export()
	if err != nil {
		log.Println("Error exporting data:", err)
	}
	jsonCompact, err := json.Marshal(state)
	if err != nil {
		log.Println("Encoding error:", err)
	}
	jsonIndent := new(bytes.Buffer)
	json.Indent(jsonIndent, jsonCompact, "", " ")
	jsonIndent.WriteByte('\n')
	jsonIndent.WriteTo(os.Stdout)
}

func List(k *NitrokeyPro, args []string) {
	switch {
	case len(args) < 1:
		ListTotp(k)
		UnlockPws(k)
		ListPws(k)
	case args[0] == "totp":
		ListTotp(k)
	case args[0] == "pws":
		UnlockPws(k)
		ListPws(k)
	default:
		ParseError("Unknown slot type \"" + args[0] + "\"\n")
	}
}

func AuthenticateUser(k *NitrokeyPro) {
	userPin := SecureInput("User Pin?")
	defer Zero(userPin)
	err := k.AuthenticateUser(userPin)
	if err != nil {
		log.Fatalln("Error authenticating user:", err.Error())
	}
}

func UnlockPws(k *NitrokeyPro) {
	userPin := SecureInput("User Pin?")
	defer Zero(userPin)
	err := k.UnlockPws(userPin)
	if err != nil {
		log.Fatalln("Error unlocking password safe:", err.Error())
	}
}

func ListPws(k *NitrokeyPro) {
	fmt.Println("Password safe slots:")
	names, _, err := k.GetPwsSlotList()
	for _, name := range names {
		fmt.Println(name)
	}
	if err != nil {
		log.Fatalln("Error reading password safe slots:", err.Error())
	}
}

func ListTotp(k *NitrokeyPro) {
	fmt.Println("Totp slots:")
	names, _, err := k.GetTotpSlotList()
	for _, name := range names {
		fmt.Println(name)
	}
	if err != nil {
		log.Fatalln("Error reading TOTP slots:", err.Error())
	}
}

func ParseError(msg string) {
	log.Println("Usage: gkey ACTION [SLOTTYPE [SLOTNAME]]")
	log.Println("  ACTION:")
	log.Println("    list [SLOTTYPE] - List slotnames")
	log.Println("    export - Print all slot information (JSON)")
	log.Println("    get SLOTTYPE SLOTNAME - Get/generate password")
	log.Println("  SLOTTYPE:")
	log.Println("    totp - Perform action on TOTP slot(s)")
	log.Println("    pws - Perform action on password safe slot(s)")
	log.Println("  SLOTNAME:")
	log.Println("    (string) - Name of slot")
	log.Fatalln("\nParse error:", msg)
}
