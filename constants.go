package main

//CommandIDs
const (
	GET_STATUS                    = 0x00
	WRITE_TO_SLOT                 = 0x01
	READ_SLOT_NAME                = 0x02
	READ_SLOT                     = 0x03
	GET_CODE                      = 0x04
	WRITE_CONFIG                  = 0x05
	ERASE_SLOT                    = 0x06
	FIRST_AUTHENTICATE            = 0x07
	AUTHORIZE                     = 0x08
	GET_PASSWORD_RETRY_COUNT      = 0x09
	CLEAR_WARNING                 = 0x0A
	SET_TIME                      = 0x0B
	TEST_COUNTER                  = 0x0C
	TEST_TIME                     = 0x0D
	USER_AUTHENTICATE             = 0x0E
	GET_USER_PASSWORD_RETRY_COUNT = 0x0F
	USER_AUTHORIZE                = 0x10
	UNLOCK_USER_PASSWORD          = 0x11
	LOCK_DEVICE                   = 0x12
	FACTORY_RESET                 = 0x13
	CHANGE_USER_PIN               = 0x14
	CHANGE_ADMIN_PIN              = 0x15
	GET_PW_SAFE_SLOT_STATUS       = 0x60
	GET_PW_SAFE_SLOT_NAME         = 0x61
	GET_PW_SAFE_SLOT_PASSWORD     = 0x62
	GET_PW_SAFE_SLOT_LOGINNAME    = 0x63
	SET_PW_SAFE_SLOT_DATA_1       = 0x64
	SET_PW_SAFE_SLOT_DATA_2       = 0x65
	PW_SAFE_ERASE_SLOT            = 0x66
	PW_SAFE_ENABLE                = 0x67
	PW_SAFE_INIT_KEY              = 0x68
	PW_SAFE_SEND_DATA             = 0x69
	SD_CARD_HIGH_WATERMARK        = 0x70
	DETECT_SC_AES                 = 0x6a
	NEW_AES_KEY                   = 0x6b
)

//CMD_STATUS_VALUES
const (
	CMD_STATUS_OK                            = 0
	CMD_STATUS_WRONG_CRC                     = 1
	CMD_STATUS_WRONG_SLOT                    = 2
	CMD_STATUS_SLOT_NOT_PROGRAMMED           = 3
	CMD_STATUS_WRONG_PASSWORD                = 4
	CMD_STATUS_NOT_AUTHORIZED                = 5
	CMD_STATUS_TIMESTAMP_WARNING             = 6
	CMD_STATUS_NO_NAME_ERROR                 = 7
	CMD_STATUS_NOT_SUPPORTED                 = 8
	CMD_STATUS_UNKNOWN_COMMAND               = 9
	CMD_STATUS_AES_DEC_FAILED                = 10
	CMD_STATUS_AES_CREATE_KEY_FAILED         = 11
	CMD_STATUS_ERROR_CHANGING_USER_PASSWORD  = 12
	CMD_STATUS_ERROR_CHANGING_ADMIN_PASSWORD = 13
	CMD_STATUS_ERROR_UNBLOCKING_PIN          = 14
)

//Status indicator
const (
	STATUS_READY           = 0x00
	STATUS_BUSY            = 0x01
	STATUS_ERROR           = 0x02
	STATUS_RECEIVED_REPORT = 0x03
)

//Password safe properties
const (
	PWS_SLOT_COUNT       = 16
	PWS_SLOTNAME_LENGTH  = 11
	PWS_PASSWORD_LENGTH  = 20
	PWS_LOGINNAME_LENGTH = 32
)

//OTP slot properties
const (
	OTP_SLOTNAME_LENGTH = 15
	OTP_TOKEN_ID_LENGTH = 13
)

//Password safe keyboard controls?
const (
	PWS_SEND_PASSWORD  = 0
	PWS_SEND_LOGINNAME = 1
	PWS_SEND_TAB       = 2
	PWS_SEND_CR        = 3
)
