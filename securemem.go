package main

/*
#include <unistd.h>
*/
import "C"
import "unsafe"
import "reflect"

// Input prompt. Remember to zero data after use.
// Subsequent calls may invalidate the slice.
func SecureInput(prompt string) (data []byte) {
	//Read password
	cbytes := unsafe.Pointer(C.getpass(C.CString(prompt)))
	//Count characters
	l := uintptr(0)
	for p := uintptr(cbytes); *(*C.char)(unsafe.Pointer(p + l)) != 0; l++ {
	}
	//Make go slice
	header := (*reflect.SliceHeader)(unsafe.Pointer(&data))
	header.Data = uintptr(cbytes)
	header.Len = int(l)
	header.Cap = int(l)
	return
}

func Zero(data ...[]byte) {
	for _, s := range data {
		for i := range s {
			s[i] = 0
		}
	}
}
